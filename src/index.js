import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';

class App extends React.Component {
  state = {
    lat: null,
    errorMessage: ''
  };

  constructor(props) {
    super(props);

    console.log('1: constructor');
  }

  renderContent() {
    var output = null;

    if (this.state.errorMessage && !this.state.lat){
      output = <div>Error: {this.state.errorMessage}</div>;
    }

    if (!this.state.errorMessage && this.state.lat) {
      output = <SeasonDisplay lat={this.state.lat} />;
    }

    return (
      output ? output : <Spinner message="Please accept location request."/>
    );
  }

  render() {
    console.log('2: render');

    return <div className="border red">{this.renderContent()}</div>;
  }

  componentDidMount() {
    console.log('3: mount');

    window.navigator.geolocation.getCurrentPosition(
      (position) => this.setState({ lat: position.coords.latitude }),
      (err) => {
        console.log(err);
        this.setState({ errorMessage: err.message });
      }
    );
  }

  componentDidUpdate() {
    console.log('4: update');
  }

  componentWillUnmount() {
    console.log('5: unmount');
  }

}

ReactDOM.render(<App />, document.querySelector('#root'))