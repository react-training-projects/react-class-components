import './SeasonDisplay.css';
import React from 'react';

const seasonConfig = {
  summer: {
    text: 'Let\'s hit the beach!',
    iconType: 'sun'
  },
  winter: {
    text: 'Brr it\'s cold!',
    iconType: 'snowflake'
  }
}

const getSeason = (lat) => {
  var month = new Date().getMonth();
  var season;

  if (month > 2 && month < 9) {
    season = lat > 0 ? 'summer' : 'winter';
  } else {
    season = lat > 0 ? 'winter' : 'summer';
  }

  return season;
}

const SeasonDisplay = (props) => {
  const season = getSeason(props.lat);
  const {text, iconType} = seasonConfig[season]

  return (
    <div className={`season-display ${season} `}>
      <i className={`${iconType} icon massive icon-left`} />
      <h1>{text}</h1>
      <i className={`${iconType} icon massive icon-right`} />
    </div>
  );
};

export default SeasonDisplay